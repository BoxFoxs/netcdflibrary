package org.satreci;

import org.junit.Test;
import ucar.nc2.NetcdfFile;

import java.io.File;
import java.io.IOException;

public class NetcdfAttrParser {

    @Test
    public void test() {
        try {
            String path = "C:\\Users\\user\\Desktop\\";
            if (path.endsWith("/") && path.endsWith("\\")) {
                path += "/";
            }
            new File(path).mkdirs();
            System.setProperty("MainDir", path);
            NetcdfFile ncFile = NetcdfFile.open("C:\\Users\\user\\Desktop\\snap\\G2011095031642.L2_COMS_OC.nc");
            System.out.println(new org.satreci.parser.NetcdfParser(ncFile).convert(path + "NetCDF_JDM.json"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
