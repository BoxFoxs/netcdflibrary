package org.satreci;

import edu.ucar.ral.nujan.netcdf.NhDimension;
import edu.ucar.ral.nujan.netcdf.NhVariable;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

public class BinaryPixelReader {
    private int dataType;
    private NhDimension[] dims;
    private FileChannel fc;
    private ByteOrder byteOrder;


    public BinaryPixelReader(FileChannel fc, int dataType, ByteOrder byteOrder, NhDimension[] dims) throws FileNotFoundException {
        this.fc = fc;
        this.dataType = dataType;
        this.byteOrder = byteOrder;
        this.dims = dims;
    }

    private int getDataUnitSize() throws IOException {
        switch (dataType) {
            case NhVariable.TP_INT:
                return Integer.BYTES;
            case NhVariable.TP_SHORT:
                return Short.BYTES;
            case NhVariable.TP_SBYTE:
                return Byte.BYTES;
            case NhVariable.TP_CHAR:
                return Character.BYTES;
            case NhVariable.TP_FLOAT:
                return Float.BYTES;
            case NhVariable.TP_DOUBLE:
                return Double.BYTES;
            case NhVariable.TP_LONG:
                return Long.BYTES;
            default:
                throw new IOException("Data type unknown");
        }
    }

    public Object readBandRasterLine(int sourceOffsetY, int sourceOffsetX, int lengthY, int lengthX) throws IOException {
        if (dims[0].getLength() <= sourceOffsetY) throw new IOException();
        long pos = (sourceOffsetY * dims[1].getLength() + sourceOffsetX) * getDataUnitSize();
        DataWrapper dataWrapper = DataWrapper.create(dataType, lengthY, lengthX);
        for (int i = 0; i < lengthY; i++) {
            ByteBuffer byteBuffer = getDataAsStream(pos + (i * dims[1].getLength() * getDataUnitSize()), lengthX);
            dataWrapper.write(byteBuffer, i);
        }
        return dataWrapper.getData();
    }

    public ByteBuffer getDataAsStream(long pos, int length) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(length * getDataUnitSize());
        fc.position(pos);
        fc.read(buffer);
        buffer.order(this.byteOrder);
        buffer.rewind();
        return buffer;
    }

    private static class DataWrapper {
        private int dataType;
        private Object dataArray;

        private static DataWrapper create(int dataType, int lengthY, int lengthX) {
            Object dataArray = null;
            switch (dataType) {
                case NhVariable.TP_INT:
                    int[][] intData = new int[lengthY][lengthX];
                    dataArray = intData;
                    break;
                case NhVariable.TP_SHORT:
                    short[][] shortData = new short[lengthY][lengthX];
                    dataArray = shortData;
                    break;
                case NhVariable.TP_SBYTE:
                    byte[][] byteData = new byte[lengthY][lengthX];
                    dataArray = byteData;
                    break;
                case NhVariable.TP_CHAR:
                    char[][] charData = new char[lengthY][lengthX];
                    dataArray = charData;
                    break;
                case NhVariable.TP_FLOAT:
                    float[][] floatData = new float[lengthY][lengthX];
                    dataArray = floatData;
                    break;
                case NhVariable.TP_DOUBLE:
                    double[][] doubleData = new double[lengthY][lengthX];
                    dataArray = doubleData;
                    break;
                case NhVariable.TP_LONG:
                    long[][] longData = new long[lengthY][lengthX];
                    dataArray = longData;
                    break;
            }
            return new DataWrapper(dataArray, dataType);
        }

        private DataWrapper(Object dataArray, int dataType) {
            this.dataArray = dataArray;
            this.dataType = dataType;
        }

        public void write(ByteBuffer buffer, int y) {
            switch (dataType) {
                case NhVariable.TP_INT:
                    buffer.asIntBuffer().get(((int[][]) dataArray)[y]);
                    break;
                case NhVariable.TP_SHORT:
                    buffer.asShortBuffer().get(((short[][]) dataArray)[y]);
                    break;
                case NhVariable.TP_SBYTE:
                    buffer.get(((byte[][]) dataArray)[y]);
                    break;
                case NhVariable.TP_CHAR:
                    buffer.asCharBuffer().get(((char[][]) dataArray)[y]);
                    break;
                case NhVariable.TP_FLOAT:
                    buffer.asFloatBuffer().get(((float[][]) dataArray)[y]);
                    break;
                case NhVariable.TP_DOUBLE:
                    buffer.asDoubleBuffer().get(((double[][]) dataArray)[y]);
                    break;
                case NhVariable.TP_LONG:
                    buffer.asLongBuffer().get(((long[][]) dataArray)[y]);
                    break;
            }
        }

        public Object getData() {
            return dataArray;
        }
    }
}