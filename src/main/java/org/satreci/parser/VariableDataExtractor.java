package org.satreci.parser;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.satreci.VariableDataIO;
import org.satreci.netcdf.DataTypeUtils;
import org.satreci.netcdf.JDMConstants;
import ucar.ma2.Array;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Dimension;
import ucar.nc2.Variable;

import java.io.IOException;
import java.util.List;

public class VariableDataExtractor {
    private final int YDIM, XDIM;
    private Variable variable;
    private final int dataType;

    public VariableDataExtractor(Variable var) {
        List<Dimension> dims = var.getDimensions();
        this.variable = var;
        this.dataType = DataTypeUtils.convertDataType(variable.getDataType().toString());
        if (dims.size() == 2) {
            YDIM = dims.get(0).getLength();
            XDIM = dims.get(1).getLength();
        } else {
            XDIM = dims.get(0).getLength();
            YDIM = -1;
        }
    }

    public JSONObject write() {
        JSONObject result = new JSONObject();
        try {
            if (YDIM > 0 && XDIM > 0) {
                String filePath = System.getProperty("MainDir") + "/" + variable.getName() + ".bin";
                VariableDataIO.VariableDataWriter writer = VariableDataIO.createWriter(filePath, dataType);
                int[] shape = variable.getShape();
                int[] origin = new int[2];
                VariableArrayData variableData = VariableArrayData.wrapping(variable.read(origin, shape));
                for (int x = 0; x < shape[0]; x++) {
                    Object[] dataArr = DataTypeUtils.createTypeArray(dataType, shape[1]);
                    for (int y = 0; y < shape[1]; y++) {
                        dataArr[y] = variableData.get(x, y);
                    }
                    writer.writeData(dataArr);
                }
                result.put(JDMConstants.DATATYPE, "File");
                result.put(JDMConstants.FILENAME, filePath);
            } else {
                Object[] datalist = readDataArray(dataType, variable.read());
                result.put(JDMConstants.DATATYPE, "Array");
                result.put(JDMConstants.DATA, arrayToJSON(datalist));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidRangeException e) {
            e.printStackTrace();
        }
        return result;
    }


    private JSONArray arrayToJSON(Object[] datalist) {
        JSONArray arr = new JSONArray();
        for (Object data : datalist) {
            arr.add(data);
        }
        return arr;
    }

    private Object[] readDataArray(int type, Array dataArray) {
        Object[] datalist = DataTypeUtils.createTypeArray(type, (int) dataArray.getSize());
        for (int i = 0; i < dataArray.getSize(); i++) {
            datalist[i] = dataArray.getObject(i);
        }
        return datalist;
    }
}
