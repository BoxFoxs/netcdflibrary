package org.satreci.parser;

import edu.ucar.ral.nujan.netcdf.NhVariable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.satreci.netcdf.DataTypeUtils;
import ucar.ma2.Array;
import ucar.ma2.ArrayInt;
import ucar.ma2.ArrayObject;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class NetcdfParser {

    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Need NetcdfFile path to argment");
            return;
        }
        try {
            String path = (args.length > 1) ? args[1] : null;
            if (path.endsWith("/") && path.endsWith("\\")) {
                path += "/";
            }
            new File(path).mkdirs();
            System.setProperty("MainDir", path);
            NetcdfFile ncFile = NetcdfFile.open(args[0]);
            System.out.println(new NetcdfParser(ncFile).convert(path + "NetCDF_JDM.json"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private NetcdfFile ncFile;

    public NetcdfParser(NetcdfFile ncFile) {
        this.ncFile = ncFile;
    }

    public JSONObject convert(String path) {
        JSONObject cdm = createGroupTree(ncFile.getRootGroup());
        if (path != null) {
            File output = new File(path);
            output.getParentFile().mkdirs();
            try {
                output.createNewFile();
                FileWriter writer = new FileWriter(output);
                writer.write(cdm.toString());
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return cdm;
    }

    public JSONObject createGroupTree(Group group) {
        JSONObject object = new JSONObject();
        object.put("Name", clearName(group.getName()));
        object.put("Attributes", createAttributeTree(group.getAttributes()));
        object.put("Variables", createVariableTree(group.getVariables()));
        object.put("Dimensions", convertDimension(group.getDimensions()));
        JSONArray subGroups = new JSONArray();
        for (Group subGroup : group.getGroups()) {
            subGroups.add(createGroupTree(subGroup));
        }
        object.put("SubGroups", subGroups);
        return object;
    }

    public JSONArray createVariableTree(List<Variable> variableList) {
        JSONArray variables = new JSONArray();
        for (Variable var : variableList) {
            JSONObject varObj = new JSONObject();
            JSONObject valueInfo = new VariableDataExtractor(var).write();
            varObj.put("Name", clearName(var.getName()));
            varObj.put("Value", valueInfo);
            varObj.put("Rank", var.getRank());
            varObj.put("SizeToCache", var.getSizeToCache());
            varObj.put("Unit", var.getUnitsString());
            varObj.put("ElementSize", var.getElementSize());
            varObj.put("Dimensions", convertDimension(var.getDimensions()));
            varObj.put("Description", var.getDescription());
            varObj.put("DataType", var.getDataType().toString());
            varObj.put("DatasetLocation", var.getDatasetLocation());
            varObj.put("Attributes", createAttributeTree(var.getAttributes()));
            variables.add(varObj);
        }
        return variables;
    }

    private JSONArray convertDimension(List<Dimension> dimensions) {
        JSONArray arr = new JSONArray();
        for (Dimension dim : dimensions) {
            JSONObject dimObj = new JSONObject();
            dimObj.put("Name", dim.getName());
            dimObj.put("Length", dim.getLength());
            arr.add(dimObj);
        }
        return arr;
    }

    public JSONArray createAttributeTree(List<Attribute> attributeList) {
        JSONArray attributes = new JSONArray();
        for (Attribute attr : attributeList) {
            JSONObject attrObj = new JSONObject();
            attrObj.put("Name", clearName(attr.getName()));
            attrObj.put("DataType", attr.getDataType().toString());
            attrObj.put("Values", arrayToJson(attr.getValues()));
            attrObj.put("isUnsigned", attr.isUnsigned());
            attributes.add(attrObj);
        }
        return attributes;
    }

    public JSONArray arrayToJson(Array values) {
        JSONArray arr = new JSONArray();
        for (int i = 0; i < values.getSize(); i++) {
            arr.add(values.getObject(i));
        }
        return arr;
    }

    private static String clearName(String name) {
        if (name.contains("/")) {
            name = name.substring(name.lastIndexOf("/") + 1, name.length());
        }
        return name;
    }

}
