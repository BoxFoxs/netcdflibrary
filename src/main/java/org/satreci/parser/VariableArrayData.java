package org.satreci.parser;

import ucar.ma2.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class VariableArrayData {
    private static final Class[] CSS = new Class[]{ArrayInt.class, ArrayDouble.class, ArrayShort.class, ArrayFloat.class, ArrayByte.class, ArrayLong.class, ArrayChar.class};
    private Object dataArrayObject;

    public static VariableArrayData wrapping(Object data) {
        VariableArrayData result = null;
        boolean check = false;
        for (Class c : CSS) {
            if (c.isInstance(data)) {
                check = true;
                break;
            }
        }
        if (check) {
            result = new VariableArrayData();
            result.dataArrayObject = data;
        }
        return result;
    }

    public Object get(int x) {
        Object result = null;
        Method method;
        try {
            method = dataArrayObject.getClass().getMethod("get", int.class);
            result = method.invoke(dataArrayObject, x);
        } catch (SecurityException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Object get(int x, int y) {
        Object result = null;
        Method method;
        try {
            method = dataArrayObject.getClass().getMethod("get", int.class, int.class);
            result = method.invoke(dataArrayObject, x, y);
        } catch (SecurityException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return result;
    }
}
