package org.satreci.writer;

import edu.ucar.ral.nujan.netcdf.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.satreci.netcdf.DataTypeUtils;
import org.satreci.netcdf.NetcdfPartWriter;

import static org.satreci.netcdf.JDMConstants.*;
import static org.satreci.netcdf.DataTypeUtils.convertArrValue;

public class G2StructurePartWriter implements NetcdfPartWriter {

    @Override
    public void encode(NhFileWriter writeable, JSONObject metadataObj) {

        writeGroup(writeable.getRootGroup(), metadataObj);
        try {
            writeDimensions(writeable.getRootGroup(), (JSONArray) metadataObj.get(DIMENSIONS));
        } catch (NhException e) {
            e.printStackTrace();
        }
    }

    private void writeGroup(NhGroup group, JSONObject dataObj) {
        try {
            writeAttribute(group, (JSONArray) dataObj.get(ATTRIBUTES));
            JSONArray subGroups = (JSONArray) dataObj.get(SUBGROUPS);
            for (int i = 0; i < subGroups.size(); i++) {
                JSONObject subGroupObj = (JSONObject) subGroups.get(i);
                writeGroup(group.addGroup((String) subGroupObj.get("Name")), subGroupObj);
            }
        } catch (NhException e) {
            e.printStackTrace();
        }
    }

    private void writeDimensions(NhGroup group, JSONArray dimensions) throws NhException {
        for (int i = 0; i < dimensions.size(); i++) {
            JSONObject dimObj = (JSONObject) dimensions.get(i);
            String name = (String) dimObj.get("Name");
            int length = (int) (long) dimObj.get("Length");
            group.addDimension(name, length);
        }
    }

    private void writeAttribute(NhGroup group, JSONArray attributes) throws NhException {
        for (int i = 0; i < attributes.size(); i++) {
            JSONObject attrObj = (JSONObject) attributes.get(i);
            String name = (String) attrObj.get("Name");
            int dataType = DataTypeUtils.convertDataType((String) attrObj.get("DataType"));
            Object value = convertArrValue(dataType, (JSONArray) attrObj.get("Values"));
            group.addAttribute(name, dataType, value);
        }
    }

}
