package org.satreci.writer;

import edu.ucar.ral.nujan.netcdf.NhException;
import edu.ucar.ral.nujan.netcdf.NhFileWriter;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.satreci.netcdf.NetcdfPartWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class G2NetcdfWriter {
    public static void main(String [] args) throws NhException {
        if (args.length < 2) {
            System.err.println("Need NetcdfFile path to argment");
            return;
        }
        File file = new File(args[0]);
        JSONObject metadataObj = getMeatadataObject(file);
        NhFileWriter writeable = new NhFileWriter(args[1], NhFileWriter.OPT_OVERWRITE);
        List<NetcdfPartWriter> writerList = Arrays.asList(new G2StructurePartWriter(), new G2BandPartWriter());
        for(NetcdfPartWriter writer : writerList){
            writer.encode(writeable, metadataObj);
        }
    }

    private static JSONObject getMeatadataObject(File file) {
        StringBuffer buffer = new StringBuffer();
        try {
            Scanner scanner = new Scanner(new FileInputStream(file));
            while (scanner.hasNext()) {
                buffer.append(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return (JSONObject) JSONValue.parse(buffer.toString());
    }
}
