package org.satreci.writer;

import edu.ucar.ral.nujan.netcdf.*;
import org.apache.commons.lang3.ArrayUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.satreci.BinaryPixelReader;
import org.satreci.netcdf.DataTypeUtils;
import org.satreci.netcdf.JDMConstants;
import org.satreci.netcdf.NetcdfPartWriter;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

import static org.satreci.netcdf.JDMConstants.*;
import static org.satreci.netcdf.DataTypeUtils.convertArrValue;

public class G2BandPartWriter implements NetcdfPartWriter {

    @Override
    public void encode(NhFileWriter writeable, JSONObject dataObject) throws NhException {
        defineVariables(writeable.getRootGroup(), dataObject);
        writeable.endDefine();
        writeVariables(writeable.getRootGroup(), dataObject);
        writeable.close();
    }

    private int[] getChunkSizes(JSONObject data) {
        if (findByName("_ChunkSizes", (JSONArray) data.get(ATTRIBUTES)) == null) return null;
        JSONArray chunkLengths = ((JSONArray) findByName("_ChunkSizes", (JSONArray) data.get(ATTRIBUTES)).get("Values"));
        int[] chunkLens = new int[chunkLengths.size()];
        for (int i = 0; i < chunkLengths.size(); i++) {
            chunkLens[i] = (int) DataTypeUtils.castingData(NhVariable.TP_INT, chunkLengths.get(i));
        }
        return chunkLens;
    }

    private Object getFillValue(int dataType, JSONObject data) {
        if (findByName("_FillValue", (JSONArray) data.get(ATTRIBUTES)) == null) return null;
        else {
            return DataTypeUtils.castingData(dataType, ((JSONArray) findByName("_FillValue", (JSONArray) data.get(ATTRIBUTES)).get("Values")).get(0));
        }
    }

    private void defineVariables(NhGroup group, JSONObject data) throws NhException {
        JSONArray variables = (JSONArray) data.get(VARIABLES);
        for (int i = 0; i < variables.size(); i++) {
            try {
                JSONObject varObj = (JSONObject) variables.get(i);
                String name = (String) varObj.get("Name");
                int dataType = DataTypeUtils.convertDataType((String) varObj.get("DataType"));
                Object fillValue = getFillValue(dataType, varObj);
                NhDimension[] nhDims = decodeDimensions(group, (JSONArray) varObj.get(DIMENSIONS));
                NhVariable variable = group.addVariable(name, dataType, nhDims, getChunkSizes(varObj), fillValue, 0);
                writeAttribute(variable, (JSONArray) varObj.get(ATTRIBUTES));
            } catch (NullPointerException np) {
                np.printStackTrace();
            }
        }
        JSONArray arr = (JSONArray) data.get(SUBGROUPS);
        for (int i = 0; i < arr.size(); i++) {
            JSONObject subGroup = (JSONObject) arr.get(i);
            defineVariables(group.findSubGroup((String) subGroup.get("Name")), subGroup);
        }
    }

    private void writeVariableData(NhVariable target, JSONObject data) throws NhException, IOException {
        JSONObject valueInfo = (JSONObject) data.get(VALUE);
        String dataType = (String) valueInfo.get(JDMConstants.DATATYPE);
        if (dataType.equals("File")) {
            ByteOrder byteOrder = getByteOrder(valueInfo);
            File file = new File((String) valueInfo.get(JDMConstants.FILENAME));
            FileChannel fc = new RandomAccessFile(file, "r").getChannel();
            BinaryPixelReader reader = new BinaryPixelReader(fc, target.getType(), byteOrder, target.getDimensions());
            writeRasterData(target, getChunkSizes(data), reader);
        } else if (dataType.equals("Array")) {
            JSONArray dataArr = (JSONArray) valueInfo.get(JDMConstants.DATA);
            if (getChunkSizes(data) != null) {
                int size = getChunkSizes(data)[0];
                for (int i = 0; i < dataArr.size() / size + ((dataArr.size() % size > 0) ? 1 : 0); i++) {
                    int length = size;
                    if (size * i + size >= dataArr.size()) {
                        length = dataArr.size() % size;
                    }
                    Object[] castedDataArr = DataTypeUtils.createTypeArray(target.getType(), length);
                    for (int j = 0; j < castedDataArr.length; j++) {
                        castedDataArr[j] = DataTypeUtils.castingData(target.getType(), dataArr.get(j + (10 * i)));
                    }
                    target.writeData(new int[]{size * i}, toPrimitive(castedDataArr), false);
                }
            } else {
                Object[] castedDataArr = DataTypeUtils.createTypeArray(target.getType(), dataArr.size());
                for (int i = 0; i < dataArr.size(); i++) {
                    castedDataArr[i] = DataTypeUtils.castingData(target.getType(), dataArr.get(i));
                }
                target.writeData(null, toPrimitive(castedDataArr), false);
            }
        }
    }

    private void writeRasterData(NhVariable target, int[] chunkLens, BinaryPixelReader reader) throws NhException, IOException {
        NhDimension[] dims = target.getDimensions();
        int[] startIxs = new int[]{0, 0};    // y, x
        while (true) {
            int chunkLenY = Math.min(chunkLens[0], dims[0].getLength() - startIxs[0]);
            int chunkLenX = Math.min(chunkLens[1], dims[1].getLength() - startIxs[1]);

            target.writeData(startIxs, reader.readBandRasterLine(startIxs[0], startIxs[1], chunkLenY, chunkLenX), false);

            for (int jj = 2 - 1; jj >= 0; jj--) {
                startIxs[jj] += chunkLens[jj];
                if (jj > 0 && startIxs[jj] >= dims[jj].getLength()) startIxs[jj] = 0;
                else break;
            }
            if (startIxs[0] >= dims[0].getLength()) break;
        }
    }

    private void writeVariables(NhGroup group, JSONObject data) throws NhException {
        JSONArray variables = (JSONArray) data.get(VARIABLES);
        for (int i = 0; i < variables.size(); i++) {
            JSONObject var = (JSONObject) variables.get(i);
            NhVariable variable = group.findVariable((String) var.get("Name"));
            if (variable == null) continue;
            try {
                writeVariableData(variable, var);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        JSONArray arr = (JSONArray) data.get(SUBGROUPS);
        for (int i = 0; i < arr.size(); i++) {
            JSONObject subGroup = (JSONObject) arr.get(i);
            writeVariables(group.findSubGroup((String) subGroup.get("Name")), subGroup);
        }
    }

    private void writeAttribute(NhVariable variable, JSONArray attributes) throws NhException {
        if (variable == null) return;
        for (int i = 0; i < attributes.size(); i++) {
            JSONObject attrObj = (JSONObject) attributes.get(i);
            String name = (String) attrObj.get("Name");
            if (name.equals("_ChunkSizes")) continue;
            int dataType = DataTypeUtils.convertDataType((String) attrObj.get("DataType"));
            Object value = convertArrValue(dataType, (JSONArray) attrObj.get("Values"));
            variable.addAttribute(name, dataType, value);
        }
    }

    private NhDimension[] decodeDimensions(NhGroup group, JSONArray dimArr) throws NhException {
        NhDimension[] dims = new NhDimension[dimArr.size()];
        for (int i = 0; i < dimArr.size(); i++) {
            JSONObject dimObj = (JSONObject) dimArr.get(i);
            String name = ((String) dimObj.get("Name"));
            int length = (int) DataTypeUtils.castingData(NhVariable.TP_INT, dimObj.get("Length"));
            dims[i] = group.findAncestorDimension(name);
            if (dims[i] == null) {
                dims[i] = group.addDimension(name, length);
            }
        }
        return dims;
    }

    private ByteOrder getByteOrder(JSONObject valueInfo) {
        String order = (String) ((valueInfo.get(JDMConstants.BYTEORDER) == null) ? "BIG" : valueInfo.get(JDMConstants.BYTEORDER));
        if (order.toLowerCase().contains("little")) {
            return ByteOrder.LITTLE_ENDIAN;
        } else {
            return ByteOrder.BIG_ENDIAN;
        }
    }

    private Object toPrimitive(Object[] castedDataArr) {
        if (castedDataArr[0] instanceof Byte) {
            byte[] dataArr = new byte[castedDataArr.length];
            for (int i = 0; i < dataArr.length; i++) {
                dataArr[i] = ((Byte) castedDataArr[i]).byteValue();
            }
            return dataArr;
        }
        return ArrayUtils.toPrimitive(castedDataArr);
    }

    private JSONObject findByName(String name, JSONArray arr) {
        for (int i = 0; i < arr.size(); i++) {
            JSONObject attr = ((JSONObject) arr.get(i));
            if (attr.get("Name").toString().equals(name)) {
                return attr;
            }
        }
        return null;
    }


}
