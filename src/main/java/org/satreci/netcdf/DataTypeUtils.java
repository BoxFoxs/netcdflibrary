package org.satreci.netcdf;

import edu.ucar.ral.nujan.netcdf.NhVariable;
import org.apache.commons.lang3.ArrayUtils;
import org.json.simple.JSONArray;
import ucar.ma2.DataType;

public class DataTypeUtils {


    public static int convertDataType(String dataTypeName) {
        switch (dataTypeName) {
            case "int":
                return NhVariable.TP_INT;
            case "float":
                return NhVariable.TP_FLOAT;
            case "double":
                return NhVariable.TP_DOUBLE;
            case "long":
                return NhVariable.TP_LONG;
            case "short":
                return NhVariable.TP_SHORT;
            case "String":
                return NhVariable.TP_STRING_VAR;
            case "byte":
                return NhVariable.TP_SBYTE;
            default:
                return -1;
        }
    }

    public static Object castingData(int type, Object obj) {
        switch (type) {
            case NhVariable.TP_INT:
                return ((Number) obj).intValue();
            case NhVariable.TP_FLOAT:
                return ((Number) obj).floatValue();
            case NhVariable.TP_DOUBLE:
                return ((Number) obj).doubleValue();
            case NhVariable.TP_LONG:
                return ((Number) obj).longValue();
            case NhVariable.TP_SHORT:
                return ((Number) obj).shortValue();
            case NhVariable.TP_SBYTE:
                return ((Number) obj).byteValue();
            default:
                return obj;
        }
    }


    public static Object castingDataAsArray(int type, Object obj) {
        switch (type) {
            case NhVariable.TP_INT:
                return new int[]{((Number) obj).intValue()};
            case NhVariable.TP_FLOAT:
                return new float[]{((Number) obj).floatValue()};
            case NhVariable.TP_DOUBLE:
                return new double[]{((Number) obj).doubleValue()};
            case NhVariable.TP_LONG:
                return new long[]{((Number) obj).longValue()};
            case NhVariable.TP_SHORT:
                return new short[]{((Number) obj).shortValue()};
            case NhVariable.TP_SBYTE:
                return new byte[]{((Number) obj).byteValue()};
            default:
                return obj;
        }
    }

    public static Object[] createTypeArray(int type, int size) {
        switch (type) {
            case NhVariable.TP_INT:
                return new Integer[size];
            case NhVariable.TP_FLOAT:
                return new Float[size];
            case NhVariable.TP_DOUBLE:
                return new Double[size];
            case NhVariable.TP_LONG:
                return new Long[size];
            case NhVariable.TP_SHORT:
                return new Short[size];
            case NhVariable.TP_SBYTE:
                return new Byte[size];
            case NhVariable.TP_STRING_VAR:
                return new String[size];
            default:
                return new Object[size];
        }
    }

    public static Object convertPrimitiveArray(int type, JSONArray arr){
        Object[] dataArr = createTypeArray(type, arr.size());
        for(int i = 0 ; i < arr.size(); i++){
            dataArr[i] = castingData(type, arr.get(i));
        }
        Object object = ArrayUtils.toPrimitive(dataArr);
        if(object instanceof Byte){

        }
        return object;
    }

    public static Object getFillValueFromDataType(int type) {
        switch (type) {
            case NhVariable.TP_INT:
                return Integer.MIN_VALUE;
            case NhVariable.TP_LONG:
                return Long.MIN_VALUE;
            case NhVariable.TP_DOUBLE:
                return Double.MIN_VALUE;
            case NhVariable.TP_FLOAT:
                return Float.MIN_VALUE;
            case NhVariable.TP_SHORT:
                return Short.MIN_VALUE;
            default:
                return Byte.MIN_VALUE;
        }
    }

    public static String getDataTypeName(int dataType){
        switch (dataType) {
            case NhVariable.TP_INT:
                return "Int";
            case NhVariable.TP_FLOAT:
                return "Float";
            case NhVariable.TP_DOUBLE:
                return "Double";
            case NhVariable.TP_LONG:
                return "Long";
            case NhVariable.TP_SHORT:
                return "Short";
            case NhVariable.TP_SBYTE:
                return "Byte";
            case NhVariable.TP_STRING_VAR:
                return "String";
            case NhVariable.TP_CHAR:
                return "Char";
            default:
                return "Unknown";
        }
    }

    public static Object convertArrValue(int dataType, JSONArray values) {
        if (values.size() == 1) {
            return DataTypeUtils.castingData(dataType, values.get(0));
        } else {
            Object[] valueList = DataTypeUtils.createTypeArray(dataType, values.size());
            for (int i = 0; i < values.size(); i++)
                valueList[i] = DataTypeUtils.castingData(dataType, values.get(i));
            return ArrayUtils.toPrimitive(valueList);
        }
    }
}
