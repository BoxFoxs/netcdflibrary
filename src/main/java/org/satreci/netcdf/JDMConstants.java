package org.satreci.netcdf;

public class JDMConstants {
    public static final String ATTRIBUTES = "Attributes";
    public static final String VARIABLES = "Variables";
    public static final String SUBGROUPS = "SubGroups";
    public static final String DIMENSIONS = "Dimensions";
    public static final String DIMENSION = "Dimension";
    public static final String VALUE = "Value";
    public static final String DATA = "Data";
    public static final String FILENAME = "FileName";
    public static final String DATATYPE = "DataType";
    public static final String BYTEORDER = "ByteOrder";
}
