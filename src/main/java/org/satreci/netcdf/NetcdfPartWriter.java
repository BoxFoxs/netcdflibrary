package org.satreci.netcdf;

import edu.ucar.ral.nujan.netcdf.NhException;
import edu.ucar.ral.nujan.netcdf.NhFileWriter;
import org.json.simple.JSONObject;

public interface NetcdfPartWriter {

    public void encode(NhFileWriter nhFileWriter, JSONObject dataObject) throws NhException;
}
