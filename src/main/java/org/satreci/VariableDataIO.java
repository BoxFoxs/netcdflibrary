package org.satreci;

import edu.ucar.ral.nujan.netcdf.NhVariable;
import org.satreci.netcdf.DataTypeUtils;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;

public class VariableDataIO {
    private Method jobMethod;

    private VariableDataIO(Method jobMethod) {
        this.jobMethod = jobMethod;
    }

    Object invokeMethod(Object obj, Object... params) {
        try {
            return jobMethod.invoke(obj, params);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

/*    public static VariableDataReader createReader(String filePath, int dataType) throws IOException {
        File file = new File(filePath);
        if (!file.exists()) throw new FileNotFoundException();
        Method jobMethod = getJobMethod(ByteBuffer.class, "read" + DataTypeUtils.getDataTypeName(dataType));
        VariableDataReader reader = new VariableDataReader(new FileInputStream(file), jobMethod, dataType);
        return reader;
    }*/

    public static VariableDataWriter createWriter(String filePath, int dataType) throws IOException {
        System.out.println(filePath);
        File file = new File(filePath);
        file.getParentFile().mkdirs();
        file.createNewFile();
        FileOutputStream fout = new FileOutputStream(file);
        Method jobMethod = getJobMethod(ByteBuffer.class, "put" + DataTypeUtils.getDataTypeName(dataType), getDataTypeClass(dataType));
        VariableDataWriter writer = new VariableDataWriter(fout, jobMethod, dataType);
        return writer;
    }

    private static Class getDataTypeClass(int dataType) throws IOException {
        switch (dataType) {
            case NhVariable.TP_INT:
                return int.class;
            case NhVariable.TP_SHORT:
                return short.class;
            case NhVariable.TP_SBYTE:
                return byte.class;
            case NhVariable.TP_CHAR:
                return char.class;
            case NhVariable.TP_FLOAT:
                return float.class;
            case NhVariable.TP_DOUBLE:
                return double.class;
            case NhVariable.TP_LONG:
                return long.class;
            case NhVariable.TP_STRING_VAR:
                return String.class;
            default:
                throw new IOException("Data type unknown");
        }
    }

    private static int getDataTypeUnitSize(int dataType) throws IOException {
        switch (dataType) {
            case NhVariable.TP_INT:
                return Integer.BYTES;
            case NhVariable.TP_SHORT:
                return Short.BYTES;
            case NhVariable.TP_SBYTE:
                return Byte.BYTES;
            case NhVariable.TP_CHAR:
                return Character.BYTES;
            case NhVariable.TP_FLOAT:
                return Float.BYTES;
            case NhVariable.TP_DOUBLE:
                return Double.BYTES;
            case NhVariable.TP_LONG:
                return Long.BYTES;
            default:
                throw new IOException("Data type unknown");
        }
    }

    private static Method getJobMethod(Class target, String methodName, Class... c) {
        Method method = null;
        try {
            method = target.getMethod(methodName, c);
        } catch (SecurityException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return method;
    }


/*    public static class VariableDataReader extends VariableDataIO {
        private DataInputStream din;
        private int dataType, unitSize;

        private VariableDataReader(DataInputStream din, Method jobMethod, int dataType) throws IOException {
            super(jobMethod);
            this.din = din;
            this.dataType = dataType;
            this.unitSize = VariableDataIO.getDataTypeUnitSize(dataType);
        }

        public Object readData() {
            return invokeMethod(din);
        }

        public Object readData(int length) {
            Object[] arr = DataTypeUtils.createTypeArray(dataType, length);
            for (int i = 0; i < arr.length; i++) {
                arr[i] = readData();
            }
            return arr;
        }

        public void close() {
            try {
                din.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }*/

    public static class VariableDataWriter extends VariableDataIO {
        private FileOutputStream fout;
        private int unitSize;

        private VariableDataWriter(FileOutputStream fout, Method jobMethod, int dataType) throws IOException {
            super(jobMethod);
            this.fout = fout;
            this.unitSize = VariableDataIO.getDataTypeUnitSize(dataType);
        }

        public void writeData(Object data) {
            ByteBuffer buffer = ByteBuffer.allocate(unitSize);
            invokeMethod(buffer, data);
            buffer.rewind();
            try {
                fout.write(buffer.array());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void writeData(Object[] dataArr) {
            ByteBuffer buffer = ByteBuffer.allocate(dataArr.length * unitSize);
            for (Object data : dataArr) {
                invokeMethod(buffer, data);
            }
            buffer.rewind();
            try {
                fout.write(buffer.array());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void close() {
            try {
                fout.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
