#Netcdf Writer 사용법

##Parser
main class : org.satreci.parser.NetcdfParser

parameter : netcdf 파일경로, 산출물 저장 경로(JSON, bin etc.)
		
		ex) C:\Users\user\Desktop\snap\G2011095031642.L2_COMS_OC.nc C:\Users\user\Desktop\

##Writer

main class : org.satreci.writer.G2NetcdfWriter

argment : json 파일 경로, 저장될 netcdf 경로
		
		ex) C:\Users\user\Desktop\NetCDF_JDM.json C:\Users\user\Desktop\COMS_GOCI_L2C_GA_2017012603161342.nc
		
		
#JSON Variable data 표기 방법

##2차원 데이터(Raster data)의 경우 다음과 같이 선언
```
{
	"Name": "aot_865",
	...
    "Value": {
		"FileName": "C:\\Users\\user\\Desktop\\\/geophysical_data\/aot_865.bin",
		"DataType": "File"
		}
	...
}
```

##1차원 데이터의 경우
```
{
	...
	"Value": {
            "DataType": "Array",
            "Data": [ 412, 443, 490, 555, 660, 680, 745, 865 ]
          }
	...
}
```     

